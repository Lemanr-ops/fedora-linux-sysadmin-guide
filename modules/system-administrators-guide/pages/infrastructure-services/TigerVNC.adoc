
:experimental:
include::{partialsdir}/entities.adoc[]

[[ch-TigerVNC]]
= TigerVNC

`TigerVNC` (Tiger Virtual Network Computing) is a system
for graphical desktop sharing which allows you to remotely control other computers.

`TigerVNC` works on the client-server network: a
*server* shares its output (`vncserver`) and a
*client* (`vncviewer`) connects to the server.

[NOTE]
====

Unlike in Fedora 15 and Red{nbsp}Hat Enterprise{nbsp}Linux{nbsp}6, `TigerVNC` in {MAJOROS} uses the `systemd` system management daemon for its configuration.
The `/etc/sysconfig/vncserver` configuration file has been replaced
by `/etc/systemd/system/vncserver@.service`.

====

[[s1-vnc-server]]
== VNC Server

`vncserver` is a utility which starts a VNC (Virtual
Network Computing) desktop. It runs [application]*Xvnc* with appropriate options and starts a window
manager on the VNC desktop. `vncserver` allows users to run
separate sessions in parallel on a machine which can then be accessed by any number of clients
from anywhere.

[[s2-vnc-installation]]
=== Installing VNC Server

To install the [application]*TigerVNC* server, issue the following command as `root`:

[subs="attributes"]
----
# dnf install tigervnc-server
----

[[s3-configuring-vnc-server]]
=== Configuring VNC Server

[[configuring-vncserver]]
.Configuring the first VNC connection
. A configuration file named `/etc/systemd/system/vncserver@.service` is required. To create this file, copy the `/lib/systemd/system/vncserver@.service` file as `root`:
+
[subs="macros, attributes"]
----

# cp /lib/systemd/system/vncserver@.service /etc/systemd/system/vncserver@.service

----
+
There is no need to include the display number in the file name because `systemd` automatically creates the appropriately named instance in memory on demand, replacing `'%i'` in the service file by the display number. For a single user it is not necessary to rename the file. For multiple users, a uniquely named service file for each user is required, for example, by adding the user name to the file name in some way. See xref:TigerVNC.adoc#configuring-vncserver-2users[Configuring VNC Server for Two Users] for details.

. Edit `/etc/systemd/system/vncserver@.service`,
  replacing _USER_ with the actual user name.
  Leave the remaining lines of the file unmodified.
  The [option]`-geometry` argument specifies the size of the VNC desktop to
  be created; by default, it is set to `1024x768`.
+
[subs="quotes, macros"]
----

ExecStart=/sbin/runuser -l _USER_ -c "/usr/bin/vncserver %i -geometry 1280x1024"
PIDFile=/home/pass:attributes[{blank}]_USER_pass:attributes[{blank}]/.vnc/%H%i.pid

----

. Save the changes.

. To make the changes take effect immediately, issue the following command:
+
[subs="macros, attributes"]
----

# pass:quotes[`systemctl daemon-reload`]

----

. Set the password for the user or users defined in the configuration file. Note
  that you need to switch from `root` to _USER_ first.
+
[subs="macros, attributes"]
----

# su - pass:quotes[_USER_]
$ pass:quotes[`vncpasswd`]
Password:
Verify:

----
+
[IMPORTANT]
====

The stored password is not encrypted; anyone who has access to the password
file can find the plain-text password.

====

Proceed to xref:TigerVNC.adoc#s4-starting-vncserver[Starting VNC Server].

[[configuring-vncserver-2users]]
==== Configuring VNC Server for Two Users

If you want to configure more than one user on the same machine,
create different template-type service files, one for each user.

. Create two service files, for example `vncserver-_USER_1_pass:attributes[{blank}]@.service`
  and `vncserver-_USER_2_pass:attributes[{blank}]@.service`.
  In both these files substitute _USER_ with the correct user name.

. Set passwords for both users:
+
[subs="macros, attributes"]
----

$ su - USER_1
$ vncpasswd
Password:
Verify:
$ su - USER_2
$ vncpasswd
Password:
Verify:

----

[[s4-starting-vncserver]]
=== Starting VNC Server

To start or enable the service, specify the display number directly in the command.
The file configured above in xref:TigerVNC.adoc#configuring-vncserver[Configuring the first VNC connection] works as a template, in which `%i` is substituted with
the display number by `systemd`.
With a valid display number, execute the following command:

[subs="attributes"]
----

# systemctl start vncserver@:display_number.service

----

You can also enable the service to start automatically at system start. Then, when you log in, `vncserver` is automatically started. As `root`, issue a command as follows:

[subs="attributes"]
----

# systemctl enable vncserver@:display_number.service

----

At this point, other users are able to use a VNC viewer program to connect to the VNC server using the display number and password defined. Provided a graphical desktop is installed, an instance of that desktop will be displayed. It will not be the same instance as that currently displayed on the target machine.

[[starting-vncserver-2displays]]
==== Configuring VNC Server for Two Users and Two Different Displays

For the two configured VNC servers, vncserver-USER_1@.service and vncserver-USER_2@.service,
you can enable different display numbers. For example, the following commands will cause a VNC server for USER_1 to
start on display 3, and a VNC server for USER_2 to start on display 5:

[subs="attributes"]
----

# systemctl start vncserver-USER_1@:3.service
# systemctl start vncserver-USER_2@:5.service

----

[[terminating-vnc-session]]
=== Terminating a VNC Session

Similarly to enabling the `vncserver` service, you can disable
the automatic start of the service at system start:

[subs="attributes"]
----

# systemctl disable vncserver@:display_number.service

----

Or, when your system is running, you can stop the service by issuing the following
command as `root`:

[subs="attributes"]
----

# systemctl stop vncserver@:display_number.service

----

[[s5-vnc-viewer]]
== VNC Viewer

`vncviewer` is the program which shows the shared graphical
user interfaces and controls the server.

For operating the `vncviewer`, there is a pop-up menu
containing entries which perform various actions such as switching in and out of
full-screen mode or quitting the viewer. Alternatively, you can operate `vncviewer`
through the terminal. Enter [command]#vncviewer -h# on the command line to list `vncviewer`pass:attributes[{blank}]'s parameters.

[[installing-vncviewer]]
=== Installing VNC Viewer

To install the [application]*TigerVNC* client, [command]#vncviewer#pass:attributes[{blank}]>, issue the following command as `root`:

[subs="attributes"]
----
# dnf install tigervnc
----

[[s6-connecting-vnc-viewer]]
=== Connecting to VNC Server

Once the VNC server is configured, you can connect to it from any VNC viewer.
In order to do so, issue the [command]#vncviewer# command in the following format:

[subs="macros"]
----
vncviewer pass:quotes[_address_]:pass:quotes[_port_number_]
----

Where _address_ is an `IP` or host name.

[[connecting-to-vncserver]]
.One Client Connecting to VNC Server
====

With the `IP` address `192.168.0.4` and display number *3*
the command looks as follows:

[subs="quotes, macros, attributes"]
----

[command]#$ vncviewer 192.168.0.4:3#

----

====

[[sec-Configuring_the_Firewall_for_VNC]]
==== Configuring the Firewall for VNC

When using a non-encrypted connection, `firewalld` might
block the connection. To allow `firewalld` to pass the VNC packets, you can open specific ports to `TCP` traffic. When using the [option]`-via` option, traffic is redirected over `SSH` which is enabled by default in `firewalld`.

[NOTE]
====

The default port of VNC server is 5900. To reach the port through which a remote
desktop will be accessible, sum the default port and the user's
assigned display number. For example, for the second port: 2 + 5900 = 5902.

====

For displays `0` to `3`, make use of `firewalld`pass:attributes[{blank}]'s support for the VNC service by means of the [option]`service` option as described below. Note that for display numbers greater than `3`, the corresponding ports will have to be opened specifically as explained in xref:TigerVNC.adoc#proc-Opening-Ports_in_firewalld[Opening Ports in firewalld].

[[proc-Enabling_VNC_Service_in_firewalld]]
.Enabling VNC Service in firewalld
. Run the following command to see the information concerning `firewalld`
  settings:
+
[subs="quotes, macros, attributes"]
----

[command]#$ firewall-cmd --list-all#

----

. To allow all VNC connections from a specific address, use a command as follows:
+
[subs="attributes"]
----
# firewall-cmd --add-rich-rule='rule family="ipv4" source address="192.168.122.116" service name=vnc-server accept'
success
----
+
See the link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Security_Guide/++[Red{nbsp}Hat Enterprise{nbsp}Linux{nbsp}7 Security Guide] for more information on the use of firewall rich language commands.

. To verify the above settings, use a command as follows:
+
[subs="attributes"]
----
# firewall-cmd --list-all
public (default, active)
  interfaces: bond0 bond0.192
  sources:
  services: dhcpv6-client ssh
  ports:
  masquerade: no
  forward-ports:
  icmp-blocks:
  rich rules:
	rule family="ipv4" source address="192.168.122.116" service name="vnc-server" accept
----

To open a specific port or range of ports make use of the [option]`--add-port` option to the [command]#firewall-cmd# command Line tool. For example, VNC display `4` requires port `5904` to be opened for `TCP` traffic.

[[proc-Opening-Ports_in_firewalld]]
.Opening Ports in firewalld
. To open a port for `TCP` traffic in the public zone, issue a command as `root` as follows:
+
[subs="attributes"]
----
# firewall-cmd --zone=public --add-port=5904/tcp
success
----

. To view the ports that are currently open for the public zone, issue a command as follows:
+
[subs="attributes"]
----
# firewall-cmd --zone=public --list-ports
5904/tcp
----

A port can be removed using the [command]#firewall-cmd --zone=pass:attributes[{blank}]_zone_ --remove-port=pass:attributes[{blank}]_number/protocol_pass:attributes[{blank}]# command.

For more information on opening and closing ports in `firewalld`, see the link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Security_Guide/++[Red{nbsp}Hat Enterprise{nbsp}Linux{nbsp}7 Security Guide].

[[s1-using_ssh]]
=== Connecting to VNC Server Using SSH

*VNC* is a clear text network protocol with no security against possible attacks on the communication. To make the communication secure, you can encrypt your server-client connection by using the [option]`-via` option. This will create an `SSH` tunnel between the VNC server and the client.

The format of the command to encrypt a VNC server-client connection is as follows:

[subs="attributes"]
----

$ vncviewer -via user@host:display_number

----

[[using-via]]
.Using the -via Option
====

. To connect to a VNC server using `SSH`, enter a command as follows:
+
[subs="attributes"]
----

$ vncviewer -via USER_2@192.168.2.101:3

----

. When you are prompted to, type the password, and confirm by pressing kbd:[Enter].

. A window with a remote desktop appears on your screen.

====

.Restricting VNC Access
If you prefer only encrypted connections, you can prevent unencrypted connections
altogether by using the [option]`-localhost` option in the `systemd.service`
file, the ExecStart line:

[subs="quotes, macros"]
----
ExecStart=/sbin/runuser -l _user_ -c "/usr/bin/vncserver -localhost %i"
----

This will stop `vncserver` from accepting connections from anything but the local host and port-forwarded connections sent using `SSH` as a result of the [option]`-via` option.

For more information on using `SSH`, see xref:infrastructure-services/OpenSSH.adoc#ch-OpenSSH[OpenSSH].

[[s9-additional-sources]]
== Additional Resources

For more information about TigerVNC, see the resources listed below.

.Installed Documentation

* `vncserver(1)` — The *VNC* server manual pages.

* `vncviewer(1)` — The *VNC* viewer manual pages.

* `vncpasswd(1)` — The *VNC* password manual pages.
